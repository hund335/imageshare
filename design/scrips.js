
function menu(login, register) {

    var loginobject = document.getElementById(login);
    var registerobject = document.getElementById(register);

    if (loginobject.style.display == "none") {
        loginobject.style.display = "block"
        registerobject.style.display = "none";
    }else{
        loginobject.style.display = "none";
        registerobject.style.display = "none";
    }
}

function zoomImage(id) {
    var box = document.getElementById('zoomedpost');

    document.getElementById('zoomedpostimage').src = document.getElementById(id).src
    document.getElementById('zoomeduserimage').src = document.getElementById('profileimage').src;
    document.getElementById('zoomedusername').innerText = document.getElementById('profilename').innerText;

    box.style.display = "block";

}

function closeImage() {
    document.getElementById('zoomedpost').style.display = "none";
}
