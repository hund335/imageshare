<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="manifest" href="http://localhost/manifest.json" />
    <meta charset="UTF-8">
    <meta name="theme-color" content="#ffffff">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="design/css/header.css">
    <script src="design/scrips.js"></script>
</head>
<body>

<div class="header">


    <div class="logindiv">
        <button onclick="menu('login', 'register')" class="login">Login</button>
        <i class="or"> or </i>
        <button onclick="menu('register', 'login')" class="login">Register</button>
    </div>

    <p class="headertitle">ImageShare</p>

    <div class="searchbox">
        <input type="text" id="search" placeholder="Search">
        <button id="searchenter">Search</button>
    </div>

</div>

<div id="login" class="loginmenu" style="display: none;">
    
    <img alt="" class="close" onclick="menu('login', 'register')" src="design/images/close.png">

    <p class="logintitle">Login</p>

    <input type="text" id="email" class="logininput" placeholder="Enter your email">

    <input type="password" id="password" class="logininput" placeholder="Enter your password">

    <button class="loginbutton">Login</button>

</div>

<div id="register" class="registermenu" style="display: none;">

    <img alt="" class="close" onclick="menu('register', 'login')" src="design/images/close.png">

    <p class="logintitle">Register</p>

    <input type="text" id="newusername" class="logininput" placeholder="Enter your username">

    <input type="text" id="newemail" class="logininput" placeholder="Enter your email">

    <input type="password" id="newpassword" class="logininput" placeholder="Enter your password">

    <input type="password" id="newpasswordconfirm" class="logininput" placeholder="Confirm your password">

    <button class="loginbutton">Register</button>

</div>

</body>
</html>