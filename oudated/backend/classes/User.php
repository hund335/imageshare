<?php

include_once $_SERVER["DOCUMENT_ROOT"]. "/backend/classes/util/Connection.php";
include_once $_SERVER["DOCUMENT_ROOT"]. "/backend/classes/exceptions/UserNotFoundException.php";

class User
{
    var $connection;

    public function __construct()
    {
        $this->connection = new Connection();
        $this->connection = $this->connection->get;

    }

    public function __destruct()
    {
        if($this->connection == true) $this->connection->close();
    }

    /*
     *  @param1 user = OUser;
     *
     * Create a new user, by creating a new OUser($username, $email, $password) object with paremeters.
     */
    function create(OUser $user): void {

        $create = $this->connection->prepare("INSERT INTO users (ID, Username, Name, Image, Email, Password) VALUES(?, ?, ?, ?, ?, ?)");
        $create->bind_param("ssssss", $user->getId(), $user->getUsername(), $user->getName(), $user->getImage(), $user->getEmail(), $user->getPassword());
        $create->execute();
        $create->close();
    }


    /*
     *  @param1 id = Int;
     *
     * Check if the user exists will return true or false
     */
    function exist(string $id): bool {

        $exist = $this->connection->prepare("SELECT COUNT(users.ID) AS exist FROM users WHERE users.ID = ?");
        $exist->bind_param("s", $id);
        $exist->execute();
        $exist->store_result();
        $exist->bind_result($sqlexist);
        $exist->fetch();
        $exist->close();

        return ($sqlexist == 1 ? true : false);

    }

    /*
     *  @param1 id = Int;
     *
     * Returns the user as an OUser object.
     */
    function get(array $dataType, $value): OUser{

        $get = $this->connection->prepare("SELECT ID, Username, Name, Image, Email, Password FROM users WHERE ". $dataType['Table'] ." = ?");
        $get->bind_param($dataType['Type'], $value);
        $get->execute();
        $get->store_result();

        if($get->num_rows < 1){

            $get->close();

            throw new UserNotFoundException("The user doesnt exists!", 1);
            return null;
        }
        $get->bind_result($sqlid,$sqlusername, $sqlname, $sqlimage, $sqlemail, $sqlpassword);
        $get->fetch();

        $user = new OUser(null, null, null);


        $user->setId($sqlid);
        $user->setUsername($sqlusername);
        $user->setName($sqlname);
        $user->setImage($sqlimage);
        $user->setEmail($sqlemail);
        $user->setPassword($sqlpassword);

        return $user;
    }

    /*
     *  @param1 user = OUser;
     *  @param2 datatype = OUserDataType;
     *  @param3 value = Optional;
     *
     * Update the user object
     */
    function update(OUser $user, array $dataType, $value): void {

        $update = $this->connection->prepare("UPDATE users SET ". $dataType['Table']. " = ? WHERE users.ID = ?");
        $update->bind_param($dataType['Type']. "s", $value, $user->getId());
        $update->execute();
        $update->close();

    }

    /*
     *  @param1 user = OUser;
     *
     * Delete the user
     */
    function delete(OUser $user): void {

        $delete = $this->connection->prepare("DELETE FROM users WHERE users.ID = ?");
        $delete->bind_param("s", $user->getId());
        $delete->execute();
        $delete->close();

    }

}