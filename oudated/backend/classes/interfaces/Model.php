<?php


interface Model
{
    public function create(): void;

    public function delete(): void;

    public function update(): void;

    public function get(): void;

}