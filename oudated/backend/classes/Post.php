<?php

include_once $_SERVER["DOCUMENT_ROOT"]. "/backend/classes/util/Connection.php";
include_once $_SERVER["DOCUMENT_ROOT"]. "/backend/classes/exceptions/PostNotFoundExecption.php";

class Post
{

    var $connection;

    public function __construct()
    {
        $this->connection = new Connection();
        $this->connection = $this->connection->get;

    }

    public function __destruct()
    {
        if($this->connection == true) $this->connection->close();
    }


    function create(OPost $post): void
    {

        $create = $this->connection->prepare("INSERT INTO posts (posts.ID, posts.User_ID, posts.Desc, posts.Date) VALUES(?, ?, ?, ?)");
        $create->bind_param("sssi", $post->getId(), $post->getAuthor(), $post->getDesc(), $post->getDate());
        $create->execute();
        $create->close();

        if(is_null($post->getImages())) return;

        $images = $post->getImages();
        $addimage = $this->connection->prepare("INSERT INTO posts_images (Post_ID, Src) VALUES(?, ?)");

        foreach ($images as $image) {

            $addimage->bind_param("ss", $post->getId(), $image->getSrc());
            $addimage->execute();

        }

        $addimage->close();

    }

    function delete(OPost $post): void {

        $delete = $this->connection->prepare("DELETE FROM posts WHERE posts.ID = ?");
        $delete->bind_param("s", $post->getId());
        $delete->execute();
        $delete->close();

    }

    function get(string $id): OPost{

        $get = $this->connection->prepare("SELECT posts.User_ID, posts.Desc, posts.Date FROM posts WHERE posts.ID = ?");
        $get->bind_param("s", $id);
        $get->execute();
        $get->store_result();

        if($get->num_rows == 0){
            $get->close();
            Throw new PostNotFoundExecption("The post doesnt exists!", 1);
            return null;
        }

        $get->bind_result( $sqluserid, $sqldesc, $sqldate);
        $get->fetch();
        $get->close();

        $post = new OPost(null, null, null);
        $post->setId($id)->setAuthor($sqluserid)->setDesc($sqldesc)->setDate($sqldate)->setImages($this->getMetaData($id));

        return $post;

    }

    private function getMetaData(string $id): array {

        $data = array();

        $metadata = $this->connection->prepare("SELECT Src FROM posts_images WHERE Post_ID = ?");
        $metadata->bind_param("s", $id);
        $metadata->execute();
        $metadata->store_result();
        $metadata->bind_result($sqlsrc);

        while($metadata->fetch()){

            $image = new OImage($sqlsrc);

            array_push($data, $image);
        }

        $metadata->close();

        return $data;
    }

    function update(OPost $post, array $dataType, $value): void {

        $update = $this->connection->prepare("UPDATE posts SET ". $dataType['Table']. " = ? WHERE posts.ID = ?");
        $update->bind_param($dataType['Type']. "s", $value, $post->getId());
        $update->execute();
        $update->close();

    }

    function getAll(int $limit, int $offset): array {

        $getall = $this->connection->prepare("SELECT ID, User_ID, posts.Desc, Date FROM Posts LIMIT ? OFFSET ?");
        $getall->bind_param("ii", $limit, $offset);
        $getall->execute();
        $getall->store_result();
        $getall->bind_result($sqlid, $sqluserid, $sqldesc, $sqldate);

        $posts = array();

        while($getall->fetch()){

            $post = new OPost(null, null, null);
            $post->setId($sqlid)->setAuthor($sqluserid)->setDesc($sqldesc)->setDate($sqldate)->setImages($this->getMetaData($sqlid));

            array_push($posts, $post);
        }

        return $posts;
    }



}