<?php

require $_SERVER["DOCUMENT_ROOT"]. '/backend/vendor/autoload.php';
use Ramsey\Uuid\Uuid;

class OUser
{

    private $id;
    private $username;
    private $name;
    private $image;
    private $email;
    private $password;

    public function __construct($username, $email, $password)
    {
        if($username == null || $email == null || $password == null) return;

        $this->id = Rhumsaa\Uuid\Uuid::uuid4()->toString();
        $this->username = $username;
        $this->name = $username;
        $this->image = "/data/default/unk529.png";
        $this->email = $email;
        $this->password = password_hash($password, PASSWORD_BCRYPT);

    }

    /**
     * @return mixed
     */
    public function &getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function &getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function &getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function &getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function &getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function &getPassword()
    {
        return $this->password;
    }

    /**
     * @param false|string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

}