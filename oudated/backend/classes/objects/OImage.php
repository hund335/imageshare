<?php


class OImage
{
    private $src;

    public function __construct($src)
    {
        if($src == null) return;
        $this->src = $src;
    }

    /**
     * @return mixed
     */
    public function &getSrc(): string
    {
        return $this->src;
    }

}