<?php


class OPost
{
    private $id;
    private $author;
    private $desc;
    private $images;
    private $date;

    public function __construct($author, $desc, $images)
    {

        if($desc == null && $author == null && $images == null) return;

        $this->id = bin2hex(openssl_random_pseudo_bytes(16));
        $this->desc = $desc;
        $this->date = getdate()[0];
        $this->author = $author;

        if($images == null) return;
        $this->images = $images;
    }


    public function &getId()
    {
        return $this->id;
    }

    public function setId($id): OPost
    {
        $this->id = $id;
        return $this;
    }

    public function &getAuthor()
    {
        return $this->author;
    }

    public function setAuthor($author): OPost
    {
        $this->author = $author;
        return $this;
    }

    public function &getDesc()
    {
        return $this->desc;
    }

    public function setDesc($desc): OPost
    {
        $this->desc = $desc;
        return $this;
    }

    public function &getImages()
    {
        return $this->images;
    }

    public function setImages($images): OPost
    {
        $this->images = $images;
        return $this;
    }

    public function &getDate()
    {
        return $this->date;
    }

    public function setDate($date): OPost
    {
        $this->date = $date;
        return $this;
    }

}