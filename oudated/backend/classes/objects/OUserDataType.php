<?php


class OUserDataType
{
    const ID = array("Table" => "ID", "Type" => "s");
    const Username = array("Table" => "Username", "Type" => "s");
    const Name = array("Table" => "Name", "Type" => "s");
    const Image = array("Table" => "Image", "Type" => "s");
    const Email = array("Table" => "Email", "Type" => "s");
    const Password = array("Table" => "Password", "Type" => "s");

}