<?php


class OPostDataType
{
    const ID = array("Table" => "posts.ID", "Type" => "s");
    const UserID = array("Table" => "posts.User_ID", "Type" => "s");
    const Desc = array("Table" => "posts.Desc", "Type" => "s");
    const Date = array("Table" => "posts.Date", "Type" => "i");

}