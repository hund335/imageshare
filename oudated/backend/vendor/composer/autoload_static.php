<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit9c9be7cc765fdb21f9c52be203bbe93c
{
    public static $files = array (
        '5255c38a0faeba867671b61dfda6d864' => __DIR__ . '/..' . '/paragonie/random_compat/lib/random.php',
    );

    public static $prefixLengthsPsr4 = array (
        'R' => 
        array (
            'Rhumsaa\\Uuid\\' => 13,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Rhumsaa\\Uuid\\' => 
        array (
            0 => __DIR__ . '/..' . '/ramsey/uuid/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit9c9be7cc765fdb21f9c52be203bbe93c::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit9c9be7cc765fdb21f9c52be203bbe93c::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
