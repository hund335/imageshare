<?php
use Illuminate\Database\Capsule\Manager as Capsule;

class Connection
{

    public function __construct()
    {

        $capsule = new Capsule;
        
        $capsule->addConnection([
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => 'imageshare',
            'username'  => 'root',
            'password'  => '',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ]);

    }


}