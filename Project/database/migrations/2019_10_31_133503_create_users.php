<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->char("id", 36)->nullable(false)->primary();
            $table->string("username", 16)->nullable(false);
            $table->string("name", 64)->nullable(false);
            $table->string("image")->nullable(false);
            $table->string("email")->nullable(false);
            $table->string("password")->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
